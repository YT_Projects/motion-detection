let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell
{
  buildInputs = [
    pkgs.deno
  ];
  shellHook =
    ''
      echo "welcom to nix shell"
      # npm install -g pnpm npm-check-updates @commitlint/cli @commitlint/config-conventional commitizen cz-conventional-changelog cz-emoji
    '';

}
